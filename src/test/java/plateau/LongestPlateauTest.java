package plateau;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestPlateauTest {

    @Test
    void printLargestPlateauLength() {
        int length;
        int start;
        Integer[] testArr = new Integer[] {1, 2, 2, 3, 2};
        LongestPlateau lp = new LongestPlateau();

        lp.printLargestPlateau(testArr);
        length = lp.getPlateauLength();

        assertEquals(1, length);
    }

    @Test
    void printLargestPlateauStart() {
        int start;
        Integer[] testArr = new Integer[] {1, 2, 2, 3, 2};
        LongestPlateau lp = new LongestPlateau();

        lp.printLargestPlateau(testArr);
        start = lp.getPlateauStart();

        assertEquals(3, start);
    }
}