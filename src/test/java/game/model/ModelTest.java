package game.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

class ModelTest {

    @Test
    public void booleanDeskNull() {
        Model m = new Model(3, 3, 1);
        assertNotNull(m.getBooleanDesk());
    }

    @Test
    public void booleanDeskContent() {
        Model m = new Model(3, 3, 1);

        boolean[][] result = new boolean[][] {
                {true, true, true},
                {true, true, true},
                {true, true, true}
        };
        assertArrayEquals(result, m.getBooleanDesk());
    }

    @Test
    public void stringDeskNull() {
        Model m = new Model(3, 3, 1);
        assertNotNull(m.getStringDesk());
    }

    @Test
    public void stringDeskContent() {
        Model m = new Model(3, 3, 1);

        String[][] result = new String[][] {
                {"*", "*", "*"},
                {"*", "*", "*"},
                {"*", "*", "*"}
        };
        assertArrayEquals(result, m.getStringDesk());
    }

    @Test
    public void countLeftBomb() {
        String[][] inDesk = new String[][] {
                {"*", "_", "_"},
                {"*", "_", "_"},
                {"*", "_", "_"}
        };
        String[][] outDesk = new String[][] {
                {"*", "1", "_"},
                {"*", "1", "_"},
                {"*", "1", "_"}
        };
        Model m = new Model(3, 3, 1);
        m.setStringDesk(inDesk);

        assertArrayEquals(outDesk, m.getCountedLeftNeighbors());
    }

    @Test
    public void countRightBomb() {
        String[][] inDesk = new String[][] {
                {"_", "*", "_"},
                {"_", "*", "_"},
                {"_", "*", "_"}
        };
        String[][] outDesk = new String[][] {
                {"1", "*", "_"},
                {"1", "*", "_"},
                {"1", "*", "_"}
        };
        Model m = new Model(3, 3, 1);
        m.setStringDesk(inDesk);

        assertArrayEquals(outDesk, m.getCountedRightNeighbors());
    }

    @Test
    public void countAboveBomb() {
        String[][] inDesk = new String[][] {
                {"*", "*", "*"},
                {"_", "_", "_"},
                {"_", "_", "_"}
        };
        String[][] outDesk = new String[][] {
                {"*", "*", "*"},
                {"1", "1", "1"},
                {"_", "_", "_"}
        };
        Model m = new Model(3, 3, 1);
        m.setStringDesk(inDesk);

        assertArrayEquals(outDesk, m.getCountedAboveNeighbors());
    }

    @Test
    public void countBelowBomb() {
        String[][] inDesk = new String[][] {
                {"_", "_", "_"},
                {"*", "*", "*"},
                {"_", "_", "_"}
        };
        String[][] outDesk = new String[][] {
                {"1", "1", "1"},
                {"*", "*", "*"},
                {"_", "_", "_"}
        };
        Model m = new Model(3, 3, 1);
        m.setStringDesk(inDesk);

        assertArrayEquals(outDesk, m.getCountedBelowNeighbors());
    }
}