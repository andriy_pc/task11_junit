package game.model.view;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

class ViewerTest {
    Viewer testView = new Viewer();
    Locale l = new Locale("en");
    @Test
    void initMenuTest() {
        boolean test = testView.initMenu(l);
        assumeTrue(test, "can't find specified locale: " + l);
    }

    @Test
    void menuLengthTest() {
        Class calzz = testView.getClass();
        try {
            Field menu = calzz.getDeclaredField("menu");
            menu.setAccessible(true);
            ArrayList<String> lst = (ArrayList) menu.get(testView);
            assertEquals(9, lst.size());
        } catch (NoSuchFieldException e) {
            System.out.println("error");
            e.printStackTrace(System.err);
        } catch (IllegalAccessException e) {
            e.printStackTrace(System.err);
        }

    }
}