package game.model;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)

class ControllerTest {

    @InjectMocks
    Controller controller;

    @Mock
    Model model;

    @Test
    void StringDesk() {
        String[][] res = new String[][]{
                {"*", "*", "*"},
                {"*", "*", "*"},
                {"*", "*", "*"}
        };
        doReturn(res).when(model).getStringDesk();
        controller = new Controller(model);
        controller.stringDesk();
        verify(model, times(1)).getStringDesk();
    }

    @RepeatedTest(5)
    void countLeft() {
        String[][] res = new String[][]{
                {"*", "*", "*"},
                {"*", "*", "*"},
                {"*", "*", "*"}
        };
        doReturn(res).when(model).getCountedLeftNeighbors();
        controller = new Controller(model);
        controller.countLeft();
        verify(model).getCountedLeftNeighbors();
    }

    @RepeatedTest(5)
    void countRight() {
        String[][] res = new String[][]{
                {"*", "*", "*"},
                {"*", "*", "*"},
                {"*", "*", "*"}
        };
        doReturn(res).when(model).getCountedRightNeighbors();
        controller = new Controller(model);
        controller.countRight();
        verify(model).getCountedRightNeighbors();
    }

    @RepeatedTest(5)
    void countAbove() {
        String[][] res = new String[][]{
                {"*", "*", "*"},
                {"*", "*", "*"},
                {"*", "*", "*"}
        };
        doReturn(res).when(model).getCountedAboveNeighbors();
        controller = new Controller(model);
        controller.countAbove();
        verify(model).getCountedAboveNeighbors();
    }

    @RepeatedTest(5)
    void countBelow() {
        String[][] res = new String[][]{
                {"*", "*", "*"},
                {"*", "*", "*"},
                {"*", "*", "*"}
        };
        doReturn(res).when(model).getCountedBelowNeighbors();
        controller = new Controller(model);
        controller.countBelow();
        verify(model).getCountedBelowNeighbors();
    }

    @RepeatedTest(10)
    void booleanDesk() {
        boolean[][] res = new boolean[][] {
                {true, true, true},
                {true, true, true},
                {true, true, true}
        };
        doReturn(res).when(model).getBooleanDesk();
        controller.booleanDesk();
        verify(model).getBooleanDesk();
    }
}