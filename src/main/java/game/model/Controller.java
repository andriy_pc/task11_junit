package game.model;

import game.model.view.Display;
import game.model.view.Viewer;

import java.util.LinkedHashMap;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Controller {
    private static Logger log = LogManager.getLogger();

    private Scanner in = new Scanner(System.in);
    private LinkedHashMap<Integer, Display> menu;
    private Model model;
    private Viewer view;

    private int M;
    private int N;
    private double p;

    public Controller(Model m) {
        model = m;
        view = new Viewer();
        menu = new LinkedHashMap<>();
        M = 3;
        N = 3;
        p = 1;
    }

    public Controller(Viewer v) {
        view = v;
        menu = new LinkedHashMap<>();
        init();
    }
    public void init() {
        log.info("Initiating controller with functions");
        menu.put(0, this::quit);
        menu.put(1, this::generate);
        menu.put(2, this::booleanDesk);
        menu.put(3, this::stringDesk);
        menu.put(4, this::countLeft);
        menu.put(5, this::countRight);
        menu.put(6, this::countAbove);
        menu.put(7, this::countBelow);
        menu.put(8, this::changeLocale);

        System.out.println("Please insert M");
        M = in.nextInt();
        System.out.println("Please insert N");
        N = in.nextInt();
        System.out.println("Please insert p(double)");
        p = Double.parseDouble(in.next());
        model = new Model(M, N, p);
    }

    public void run() {
        view.display();
        int choice = in.nextInt();
        menu.get(choice).display();
    }

    public void quit() {
        log.info("quit");
        System.exit(0);
    }

    public void generate() {
        log.info("stubbed method");
        System.out.println("desk was generated");
    }

    public void booleanDesk() {
        log.info("getting boolean desk");
        boolean[][] dsk = model.getBooleanDesk();
        for(int i = 0; i < M; ++i) {
            for(int j = 0; j < N; ++j) {
                System.out.print(dsk[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void stringDesk() {
        log.info("getting string desk");
        String[][] dsk = model.getStringDesk();
        for(int i = 0; i < M; ++i) {
            for(int j = 0; j < N; ++j) {
                System.out.print(dsk[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void countLeft() {
        log.info("get counted left neighbors");
        String[][] dsk = model.getCountedLeftNeighbors();
        for(int i = 0; i < M; ++i) {
            for(int j = 0; j < N; ++j) {
                System.out.print(dsk[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void countRight() {
        log.info("get counted right neighbors");
        String[][] dsk = model.getCountedRightNeighbors();
        for(int i = 0; i < M; ++i) {
            for(int j = 0; j < N; ++j) {
                System.out.print(dsk[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void countAbove() {
        log.info("get counted above neighbors");
        String[][] dsk = model.getCountedAboveNeighbors();
        for(int i = 0; i < M; ++i) {
            for(int j = 0; j < N; ++j) {
                System.out.print(dsk[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void countBelow() {
        log.info("get counted below neighbors");
        String[][] dsk = model.getCountedBelowNeighbors();
        for(int i = 0; i < M; ++i) {
            for(int j = 0; j < N; ++j) {
                System.out.print(dsk[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void changeLocale() {
        log.info("changing locale");
        System.out.println("Please choose locale");
        view.displayLocale();
        int choice = in.nextInt();
        view.initLocale(choice);
    }
}
