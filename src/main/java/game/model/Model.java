package game.model;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Model {
    private static int M = 5;
    private static int N = 5;
    private static double p = 0.5;
    private static Logger log = LogManager.getLogger();

    private boolean[][] booleanDesk;
    private String[][] stringDesk;
    private String[][] changedStringDesk;

    public Model(int m, int n, double p) {
        M = m;
        N = n;
        this.p = p;
        booleanDesk = new boolean[M][N];
        stringDesk = new String[M][N];
        changedStringDesk = new String[M][N];
        initDesk();
    }
    private void initDesk() {
        log.info("initiating desks(boolean, string)");
        for(int i = 0; i < M; ++i) {
            for(int j = 0; j < N; ++j) {
                booleanDesk[i][j] = Math.random() >= p ? false : true;
                stringDesk[i][j] = booleanDesk[i][j] == true ? "*" : "_";
            }
        }
    }
    private void countLeftNeighbor() {
        log.info("counting left neighbors");
        changedStringDesk = stringDesk;
        for(int i = 0; i < M; ++i) {
            for(int j = 1; j < N; ++j) {
                if(!changedStringDesk[i][j].equals("*")) {
                    changedStringDesk[i][j] = changedStringDesk[i][j-1].equals("*") ? "1" : "_";
                }
            }
        }
    }
    private void countRightNeighbor() {
        log.info("counting right neighbors");
        changedStringDesk = stringDesk;
        for(int i = 0; i < M; ++i) {
            for(int j = 0; j < N-1; ++j) {
                if(!stringDesk[i][j].equals("*")) {
                    changedStringDesk[i][j] = stringDesk[i][j+1].equals("*") ? "1" : "_";
                }
            }
        }
    }
    private void countAboveNeighbor() {
        log.info("counting above neighbors");
        changedStringDesk = stringDesk;
        for(int i = 1; i < M; ++i) {
            for(int j = 0; j < N; ++j) {
                if(!stringDesk[i][j].equals("*")) {
                    changedStringDesk[i][j] = stringDesk[i-1][j].equals("*") ? "1" : "_";
                }
            }
        }
    }
    private void countBelowNeighbor() {
        log.info("counting below neighbors");
        changedStringDesk = stringDesk;
        for(int i = 0; i < M-1; ++i) {
            for(int j = 0; j < N; ++j) {
                if(!stringDesk[i][j].equals("*")) {
                    changedStringDesk[i][j] = stringDesk[i+1][j].equals("*") ? "1" : "_";
                }
            }
        }
    }

    public void display() {
        for(int i = 0; i < M; i++) {
            for(int j = 0; j < N; j++) {
                System.out.print(changedStringDesk[i][j] + " ");
            }
            System.out.println();
        }
    }

    public String[][] getCountedLeftNeighbors() {
        countLeftNeighbor();
        return stringDesk;
    }
    public String[][] getCountedRightNeighbors() {
        countRightNeighbor();
        return changedStringDesk;
    }
    public String[][] getCountedAboveNeighbors() {
        countAboveNeighbor();
        return changedStringDesk;
    }
    public String[][] getCountedBelowNeighbors() {
        countBelowNeighbor();
        return changedStringDesk;
    }
    public boolean[][] getBooleanDesk() {
        return booleanDesk;
    }
    public String[][] getStringDesk() {
        return stringDesk;
    }
    public void setStringDesk(String[][] desk) {
        stringDesk = desk;
    }
}
