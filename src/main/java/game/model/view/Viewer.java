package game.model.view;

import java.util.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Viewer {
    private ArrayList<String> menu;
    private LinkedHashMap<Integer, Locale> locales;
    private static Logger log = LogManager.getLogger();

    public Viewer() {
        locales = new LinkedHashMap<>();
        locales.put(1, new Locale("uk"));
        locales.put(2, new Locale("en"));
        initLocale(1);
    }

    protected boolean initMenu(Locale locale) {
        log.info("Initiating viewer's menu");
        menu = new ArrayList<>();
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("menu", locale);
            menu.add(bundle.getString("0"));
            menu.add(bundle.getString("1"));
            menu.add(bundle.getString("2"));
            menu.add(bundle.getString("3"));
            menu.add(bundle.getString("4"));
            menu.add(bundle.getString("5"));
            menu.add(bundle.getString("6"));
            menu.add(bundle.getString("7"));
            menu.add(bundle.getString("8"));
            return true;
        } catch(MissingResourceException e) {
            log.error("MissingResourceException. Can't " +
                    "find properties file for current locale");
            return false;
        }
    }
    public boolean initLocale(int choice) {
        try {
            initMenu(locales.get(choice));
            log.info("initiating locale");
            return true;
        } catch(IndexOutOfBoundsException e) {
            log.error("out of bounds in locales list");
            return false;
        }

    }
    public void display() {
        for(String s : menu) {
            System.out.println(s);
        }
    }
    public void displayLocale() {
        for(int i : locales.keySet()) {
            System.out.println(i + " " + locales.get(i));
        }
    }
}
