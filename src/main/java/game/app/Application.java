package game.app;

import game.model.*;
import game.model.view.Viewer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger log = LogManager.getLogger();
    public static void main(String[] args) {
        log.info("starting application");
        Controller c = new Controller(new Viewer());
        while(true) {
            c.run();
        }
    }
}
