package plateau;

public class LongestPlateau {
    private int plateauStart;
    private int plateauLength;
    public void printLargestPlateau(Integer[] values) {
        int biggestStartIndex = -1;
        int biggestLength = 0;
        int currentIndex = 1;
        int currentPlateauStartIndex = 1;
        int currentLength = 1;
        boolean plateauStarted = false;
        while (currentIndex < values.length) {
            if (isStartOfPlateau(currentIndex, values)) {
                plateauStarted = true;
                currentPlateauStartIndex = currentIndex;
                currentLength = 1;
            } else if (isEndOfPlateau(currentIndex, values)) {
                if (plateauStarted && currentLength > biggestLength) {
                    biggestLength = currentLength;
                    biggestStartIndex = currentPlateauStartIndex;
                }
                plateauStarted = false;
                currentLength = 1;
            } else {
                currentLength++;
            }
            currentIndex++;
        }
        if (biggestStartIndex < 0) {
            System.out.println("No plateau");
        } else {
            setPleatue(biggestStartIndex, currentIndex);
        }

        display();
    }

    private  boolean isStartOfPlateau(int index, Integer[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] < values[index];
    }

    private boolean isEndOfPlateau(int index, Integer[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] > values[index];
    }

    private void setPleatue(int index, int currentIndex) {
        plateauStart = index;
        plateauLength = currentIndex - index - 1;
    }

    private void display() {
        System.out.println("plateau starts at: " + plateauStart);
        System.out.println("it's length = " + plateauLength);
    }
    public int getPlateauLength() {
        return plateauLength;
    }
    public int getPlateauStart() {
        return plateauStart;
    }
}
