package plateau;

import java.util.ArrayList;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println(
                "insert integers to find longest plateau\n" +
                "(use whitespace to separate);\n");
        ArrayList<Integer> l = new ArrayList<>();
        String choice = in.nextLine();
        for(String s : choice.split(" ")) {
            l.add(Integer.parseInt(s));
        }
        LongestPlateau lp = new LongestPlateau();
        Integer[] ar = l.toArray(new Integer[0]);
        lp.printLargestPlateau(ar);
    }
}
